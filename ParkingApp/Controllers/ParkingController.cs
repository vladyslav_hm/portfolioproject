﻿using ParkingApp.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Timers;

namespace ParkingApp.Controllers
{
    public class ParkingController
    {
        private Parking parking = Parking.GetInstance();

        /// <summary>
        /// Узнать текущий баланс Парковки.
        /// </summary>
        /// <returns></returns>
        public string GetParkingBalance()
        {
            return $"Parking balance: {parking.Balance}";
        }

        /// <summary>
        /// Сумму заработанных денег за последнюю минуту.
        /// </summary>
        /// <returns></returns>
        public string GetProfit()
        {
            return $"Parking profit: {parking.Transactions.Sum(s => s.WithdrawnFunds)}";
        }

        /// <summary>
        /// Узнать количество свободных/занятых мест на парковке.
        /// </summary>
        /// <returns></returns>
        public string GetPlaceStatus()
        {
            (int free, int busy) placeStatus = parking.GetPlaceStatus();
            return $"Free {placeStatus.free}  Busy {placeStatus.busy}";
        }

        /// <summary>
        /// Вывести на экран все Транзакции Парковки за последнюю минуту
        /// </summary>
        /// <returns></returns>
        public string GetTransactions()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("Transactions:\n");
            foreach (Transaction transaction in parking.Transactions)
            {
                sb.Append(transaction);
                sb.Append("\n");
            }
            return sb.ToString();
        }

        /// <summary>
        /// Вывести всю историю Транзакций (считав данные из файла Transactions.log)
        /// </summary>
        /// <returns></returns>
        public string GetTransactionsHistory()
        {
            try
            {
                StringBuilder sb = new StringBuilder();

                sb.Append("Transactions from log:\n");
                foreach (Transaction transaction in parking.ReadTransactionHisory())
                {
                    sb.Append(transaction);
                    sb.Append("\n");
                }

                return sb.ToString();
            }
            catch (FileNotFoundException fileNotFoundException)
            {
                return $"FileNotFoundException {fileNotFoundException.Message}";
            }
            catch (Exception e)
            {
                return $"Exception {e.Message}";
            }
        }

        /// <summary>
        /// Вывести на экран список всех Транспортных средств
        /// </summary>
        /// <returns></returns>
        public string GetAllVehicleOnParking()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("All Vehicle on Parking:\n");
            foreach (ParkingPlace parkingPlace in parking.ParkingPlaces.Where(s => s.Vehicle != null))
            {
                sb.Append(parkingPlace.Vehicle);
                sb.Append("\n");
            }
            return sb.ToString();
        }

        /// <summary>
        /// Создать/поставить Транспортное средство на Парковку
        /// </summary>
        /// <param name="vehicleType"></param>
        public string AddVehicleInParking(string vehicleType)
        {
            try
            {
                if (byte.TryParse(vehicleType, out byte typeResult))
                {
                    var selectedRateEntry = GlobalSettings.PARKINGR_RATES?.FirstOrDefault(s => s.Numer == typeResult);

                    if (selectedRateEntry != null && selectedRateEntry.Value.Type != null)
                    {
                        Type selectedVehicleType = selectedRateEntry.Value.Type;
                        VehicleBaseModel vehicleBaseModel = (VehicleBaseModel)Activator.CreateInstance(selectedVehicleType);
                        vehicleBaseModel.VehicleName = selectedVehicleType.Name;

                        VehicleBaseModel addedVehicleBaseModel = parking.AddVehicle(vehicleBaseModel);
                        if (addedVehicleBaseModel != null)
                        {
                            return $"Added Vehicle {addedVehicleBaseModel.ToString()}";
                        }
                    }
                }
                return "Nothing added";
            }
            catch (ArgumentException argumentException)
            {
                return $"ArgumentException {argumentException.Message}";
            }
            catch (FormatException formatException)
            {
                return $"FormatException {formatException.Message}";
            }
            catch (Exception e)
            {
                return $"Exception {e.Message}";
            }
        }

        /// <summary>
        /// Удалить/забрать Транспортное средство с Парковки
        /// </summary>
        /// <param name="vehicleBaseModelId"></param>
        public string DeleteVehicleFromParking(string vehicleBaseModelId)
        {
            try
            {
                Guid vehicleBaseModelGuid = Guid.Parse(vehicleBaseModelId);
                VehicleBaseModel deletedVehicleBaseModel = parking.RemoveVehicle(vehicleBaseModelGuid);
                if (deletedVehicleBaseModel != null)
                {
                    return $"Deleted Vehicle {deletedVehicleBaseModel.ToString()}";
                }
                return "Nothing deleted";
            }
            catch (ArgumentException argumentException)
            {
                return $"ArgumentException {argumentException.Message}";
            }
            catch (FormatException formatException)
            {
                return $"FormatException {formatException.Message}";
            }
            catch (Exception e)
            {
                return $"Exception {e.Message}";
            }
        }

        /// <summary>
        /// Пополнить баланс конкретного Транспортного средства
        /// </summary>
        /// <param name="vehicleId_Funds"></param>
        public string TopUpVehicleBalance(string vehicleId_Funds)
        {
            try
            {
                string[] separetedStrings = vehicleId_Funds.Split(GlobalSettings.LOG_SEPARATOR_CHAR);
                Guid parseGuid = Guid.Parse(separetedStrings[0]);

                if (float.TryParse(separetedStrings[1], out float parseFunds))
                {
                    VehicleBaseModel topUpVehicleBaseModel = parking.TopUpVehicleBalance(parseGuid, parseFunds);
                    if (topUpVehicleBaseModel != null)
                        return $"Balance topUped on {parseFunds} \n{topUpVehicleBaseModel.ToString()}";
                }

                return "Nothing to topUp";

            }
            catch (ArgumentException argumentException)
            {
                return $"ArgumentException {argumentException.Message}";
            }
            catch (FormatException formatException)
            {
                return $"FormatException {formatException.Message}";
            }
            catch (Exception e)
            {
                return $"Exception {e.Message}";
            }
        }


        public string GetAllAvailableVehicle()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in GlobalSettings.PARKINGR_RATES)
            {
                sb.Append(item.Numer);
                sb.Append(" - ");
                sb.Append(item.Type.Name);
                sb.Append("\n");
            }
            return sb.ToString();
        }
    }
}
