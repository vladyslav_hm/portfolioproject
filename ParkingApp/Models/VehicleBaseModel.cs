﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using ParkingApp.Interfaces;

namespace ParkingApp.Models
{
    public abstract class VehicleBaseModel
    {
        public Guid Id { get; } = System.Guid.NewGuid();
        public string VehicleName { get; set; }
        public float Balance { get; set; }

        public event EventHandler<float> OnPayParking;
        public bool PayParking(float foundToPay)
        {          
            this.Balance -= foundToPay;
            OnPayParking?.Invoke(this, foundToPay);
            if (this.Balance < 0 )
            {
                return false;
            }
            return true;
        }

        public override string ToString()
        {
            return $"{Id}   {VehicleName}   {Balance}";
        }
    }
}
