﻿using ParkingApp.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingApp.Models.VehicleModels
{
    internal class Motorcycle : VehicleBaseModel, IVehicle
    {      
        public void SaySomething()
        {
            Console.WriteLine("Im motorcycle, I can make backFlip");
        }
    }
}
