﻿using System;
using System.Collections.Generic;
using System.Text;
using ParkingApp.Interfaces;

namespace ParkingApp.Models.VehicleModels
{
    internal class Truck : VehicleBaseModel, IVehicle
    {     
        public void SaySomething()
        {
            Console.WriteLine("Im truck, I can freight transportation");
        }
    }
}
