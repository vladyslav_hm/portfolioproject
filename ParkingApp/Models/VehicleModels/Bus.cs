﻿using ParkingApp.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingApp.Models.VehicleModels
{
    internal class Bus : VehicleBaseModel, IVehicle
    {      
        public void SaySomething()
        {
            Console.WriteLine("Im bus, I can drive with many passengers");
        }
    }
}
