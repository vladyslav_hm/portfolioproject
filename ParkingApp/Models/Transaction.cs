﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingApp.Models
{
    public class Transaction
    {
        private Guid Id { get; set; } = System.Guid.NewGuid();
        public long TimeStamp { get; private set; } = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
        public Guid VehicleId { get; private set; }
        public string VehicleName { get; private set; }
        public float WithdrawnFunds { get; private set; }

        public Transaction(Guid vehicleId,string vehicleName, float withdrawnFunds)
        {
            this.VehicleId = vehicleId;
            this.VehicleName = vehicleName;
            this.WithdrawnFunds = withdrawnFunds;
        }

        public Transaction(string logLine)
        {
            string[] spitedString = logLine.Split(GlobalSettings.LOG_SEPARATOR_CHAR);
            this.Id = Guid.Parse(spitedString[0]);
            this.TimeStamp = long.Parse(spitedString[1]);
            this.VehicleId = Guid.Parse(spitedString[2]);
            this.VehicleName = spitedString[3];
            this.WithdrawnFunds = float.Parse(spitedString[4]);
        }

        public override string ToString()
        {
            return $"{Id}{GlobalSettings.LOG_SEPARATOR_CHAR}" +
                    $"{TimeStamp}{GlobalSettings.LOG_SEPARATOR_CHAR}" +
                    $"{VehicleId}{GlobalSettings.LOG_SEPARATOR_CHAR}" +
                    $"{VehicleName}{GlobalSettings.LOG_SEPARATOR_CHAR}" +
                    $"{WithdrawnFunds}";
        }
    }
}
