﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace ParkingApp.Models
{
    class Parking
    {
        public float Balance { get; private set; }
        public List<Transaction> Transactions { get; private set; } = new List<Transaction>();
        public List<ParkingPlace> ParkingPlaces { get; private set; } = new List<ParkingPlace>(GlobalSettings.PARKING_MAX_CAPACITY);

        public static Parking GetInstance()
        {          
            return Instance.Value;
        }

        public void TopUpBalanceParking(object sender, float founds)
        {
            if (sender is VehicleBaseModel vehicleBaseModel)
            {
                Transaction transaction = new Transaction(vehicleBaseModel.Id, vehicleBaseModel.VehicleName, founds);
                Transactions.Add(transaction);
                this.Balance += founds;
            }
        }

        public VehicleBaseModel AddVehicle(VehicleBaseModel vehicleBaseModel)
        {
            ParkingPlace foundFreeParkingPlace = ParkingPlaces.FirstOrDefault(s => s.Vehicle == null);

            if (foundFreeParkingPlace != null)
            {
                float rateForVehicleBaseModel = GetRateForVehicle(vehicleBaseModel);
                if (rateForVehicleBaseModel >= 0)
                {
                    vehicleBaseModel.OnPayParking += TopUpBalanceParking;
                    foundFreeParkingPlace.Rate = rateForVehicleBaseModel;
                    foundFreeParkingPlace.Vehicle = vehicleBaseModel;
                    return vehicleBaseModel;
                }
            }
            return null;
        }

        public VehicleBaseModel RemoveVehicle(Guid vehicleBaseModelIdGuid)
        {
            ParkingPlace deleteParkingPlace = ParkingPlaces.FirstOrDefault(s => s.Vehicle.Id == vehicleBaseModelIdGuid);

            if (deleteParkingPlace != null)
            {
                VehicleBaseModel deleteVehicleBaseModel = deleteParkingPlace.Vehicle;
                deleteParkingPlace.Vehicle.OnPayParking -= TopUpBalanceParking;
                deleteParkingPlace.Vehicle = null;
                deleteParkingPlace.Rate = 0;
 
                return deleteVehicleBaseModel;
            }
            return null;
        }

        public (int free, int busy) GetPlaceStatus()
        {
            return (ParkingPlaces.Count(s => s.Vehicle == null), ParkingPlaces.Count(s => s.Vehicle != null));
        }

        public VehicleBaseModel TopUpVehicleBalance(Guid vehicleBaseModelIdGuid, float funds)
        {
            ParkingPlace parkingPlace = ParkingPlaces.FirstOrDefault(s => s.Vehicle.Id == vehicleBaseModelIdGuid);

            if (parkingPlace != null)
            {
                VehicleBaseModel vehicleToTopUp = parkingPlace.Vehicle;
                vehicleToTopUp.Balance += funds;
                return vehicleToTopUp;
            }
            return null;
        }

        public IEnumerable<Transaction> ReadTransactionHisory()
        {
            using (System.IO.StreamReader sr = new System.IO.StreamReader(GlobalSettings.PATH_LOG_FILE))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    yield return new Transaction(line);
                }
            }
        }

        private static readonly Lazy<Parking> Instance = new Lazy<Parking>(()=>new Parking());     
        private Timer parkingPayment = new Timer();
        private Timer cleanUpOldTransactions = new Timer();
        private Timer loggerTransactions = new Timer();

        private Parking()
        {
            for (int i = 0; i < GlobalSettings.PARKING_MAX_CAPACITY; i++)
            {
                ParkingPlaces.Add(new ParkingPlace());
            }

            parkingPayment.Interval = GlobalSettings.PARKING_PAYMENT_TIME * 1000;
            parkingPayment.Elapsed += ParkingPayment_Elapsed;
            parkingPayment.Start();

            cleanUpOldTransactions.Interval = GlobalSettings.PARKING_CLAENUP_INTERVAL * 1000;
            cleanUpOldTransactions.Elapsed += CleanUpOldTransactions_Elapsed;
            cleanUpOldTransactions.Start();

            loggerTransactions.Interval = GlobalSettings.PARKING_LOGGER_INTERVAL * 1000;
            loggerTransactions.Elapsed += LoggerTransactions_Elapsed;
            loggerTransactions.Start();
        }

        private float GetRateForVehicle(VehicleBaseModel vehicleBaseModel)
        {
            var selectedRateEntry = GlobalSettings.PARKINGR_RATES?.FirstOrDefault(s => s.Type == vehicleBaseModel.GetType());

            if (selectedRateEntry != null)
            {
                return selectedRateEntry.Value.Rate;
            }
            return -1;
        }

        private void ParkingPayment_Elapsed(object sender, ElapsedEventArgs e)
        {
            foreach (ParkingPlace parkingPlace in ParkingPlaces.Where(s => s.Vehicle != null))
            {
                bool isEnoughMoney = parkingPlace.Vehicle.PayParking(parkingPlace.Rate);

                if (!isEnoughMoney && !parkingPlace.IsPenaltyRate)
                {
                    parkingPlace.Rate *= GlobalSettings.PARKING_PENALTY_COEF;
                    parkingPlace.IsPenaltyRate = true;
                }
                else if (isEnoughMoney && parkingPlace.IsPenaltyRate)
                {
                    parkingPlace.Rate = GetRateForVehicle(parkingPlace.Vehicle);
                    parkingPlace.IsPenaltyRate = false;
                }
            }
        }

        private void CleanUpOldTransactions_Elapsed(object sender, ElapsedEventArgs e)
        {
            Transactions.RemoveAll(s => s.TimeStamp <= DateTimeOffset.UtcNow.ToUnixTimeSeconds() - GlobalSettings.PARKING_TRANSACTION_TIME_CAPACITY);
        }

        private void LoggerTransactions_Elapsed(object sender, ElapsedEventArgs e)
        {
            using (System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(GlobalSettings.PATH_LOG_FILE))
            {
                foreach (var item in Transactions.OrderBy(s => s.TimeStamp))
                {
                    streamWriter.WriteLine(item);
                }
            }
        }
    }
}
