﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingApp.Models
{
    public class ParkingPlace
    {
        public VehicleBaseModel Vehicle { get; set; }
        public float Rate { get; set; }
        public bool IsPenaltyRate { get; set; } = false;
    }
}
