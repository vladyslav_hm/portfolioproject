﻿using ParkingApp.Controllers;
using ParkingApp.Interfaces;
using ParkingApp.Models;
using ParkingApp.Models.UserCommandModels;
using ParkingApp.Models.VehicleModels;
using System;
using System.Collections.Generic;

namespace ParkingApp
{
    class Program
    {
        static void Main(string[] args)
        {
            ParkingController parkingController = new ParkingController();
            Commands commands = new Commands(parkingController);

            try
            {
                bool isStart = true;

                while (true)
                {
                    string readLine = (isStart) ? "0" : Console.ReadLine();

                    if (isStart) isStart = false;

                    byte codeOperation = Convert.ToByte(readLine);

                    var selectedOperation = commands.menuItems.Find(s => s.HotKey.Equals(codeOperation));

                    if (selectedOperation == null) throw new ArgumentException("Unknown command");
                    if (selectedOperation.MakerFuncHandler == null)
                        Console.WriteLine(selectedOperation.DoGetterFuncHandler());
                    else
                    {
                        Console.WriteLine(selectedOperation.DoGetterFuncHandler());
                        readLine = Console.ReadLine();
                        Console.WriteLine(selectedOperation.DoMakerFuncHandler(readLine));
                    }
                }
            }
            catch (FormatException formatException)
            {
                Console.WriteLine($"FormatExeption {formatException.Message}");
            }
            catch (ArgumentException argumentException)
            {
                Console.WriteLine($"ArgumentExeption {argumentException.Message}");
            }
            catch (Exception exception)
            {
                Console.WriteLine($"GeneralExeption {exception.Message}");
            }

            Console.ReadKey();
        }
    }
}
